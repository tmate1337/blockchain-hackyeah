package com.innobie.blockchain.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.innobie.blockchain.request.CsvRequest;
import com.innobie.blockchain.service.api.CsvHandlerService;

@CrossOrigin
@RestController
@RequestMapping("/csv")
public class CsvUploadController {

	@Autowired
	private CsvHandlerService csvHandlerService;

	@PostMapping(value = "/upload", consumes = "multipart/form-data")
	public ResponseEntity<List<CsvRequest>> handleFileUpload(@RequestParam("file") MultipartFile file) {

		try {
			return new ResponseEntity<>(csvHandlerService.readFile(file), HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
