package com.innobie.blockchain.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.innobie.blockchain.entity.Transaction;
import com.innobie.blockchain.request.CsvRequest;
import com.innobie.blockchain.service.api.TransactionService;

@CrossOrigin
@RestController
@RequestMapping("/transaction")
public class TransactionController {

	@Autowired
	private TransactionService transactionService;

	@PostMapping("/")
	public String createTransaction(@RequestBody CsvRequest request) {
		try {
			return transactionService.createNewTransaction(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("/{hash}")
	public Transaction getTransactionByHash(@PathVariable(value = "hash") String hash) {
		return transactionService.getTransactionByHash(hash);
	}

	@GetMapping("/history/{client}")
	public List<Transaction> getTransactionHistory(@PathVariable(value = "client") String client) {
		return transactionService.getTransactionHistory(client);
	}

}
