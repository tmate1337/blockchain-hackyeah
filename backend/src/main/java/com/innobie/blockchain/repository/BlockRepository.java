package com.innobie.blockchain.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.innobie.blockchain.entity.Block;

public interface BlockRepository extends JpaRepository<Block, Integer> {

	Block findFirstByOrderByBlockIdDesc();

	Optional<Block> findByHash(String hash);

}
