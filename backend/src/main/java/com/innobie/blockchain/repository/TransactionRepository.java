package com.innobie.blockchain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.innobie.blockchain.entity.Transaction;
import com.innobie.blockchain.entity.User;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

	List<Transaction> findAllByUser(User client);

}
