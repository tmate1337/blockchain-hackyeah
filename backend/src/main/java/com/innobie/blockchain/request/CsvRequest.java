package com.innobie.blockchain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CsvRequest {
	@JsonProperty
	private final String transactionTime;
	@JsonProperty
	private final String clientId;
	@JsonProperty
	private final String points;

	public CsvRequest(String line) {
		String[] split = line.split(";");
		transactionTime = split[0];
		clientId = split[1];
		points = split[2];
	}

	public CsvRequest(String transactionTime, String clientId, String points) {
		this.transactionTime = transactionTime;
		this.clientId = clientId;
		this.points = points;
	}

}
