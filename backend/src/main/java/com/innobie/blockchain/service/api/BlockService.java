package com.innobie.blockchain.service.api;

import com.innobie.blockchain.entity.Block;
import com.innobie.blockchain.entity.Transaction;
import com.innobie.blockchain.request.CsvRequest;

public interface BlockService {

	Block initBlock(CsvRequest request, Transaction transaction) throws Exception;

	Block createBlock(CsvRequest request, Transaction transaction) throws Exception;

}
