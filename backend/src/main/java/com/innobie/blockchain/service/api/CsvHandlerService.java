package com.innobie.blockchain.service.api;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.innobie.blockchain.request.CsvRequest;

public interface CsvHandlerService {

	List<CsvRequest> readFile(MultipartFile file) throws Exception;

}
