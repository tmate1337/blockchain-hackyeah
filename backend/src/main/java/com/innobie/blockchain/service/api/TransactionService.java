package com.innobie.blockchain.service.api;

import java.util.List;

import com.innobie.blockchain.entity.Transaction;
import com.innobie.blockchain.entity.User;
import com.innobie.blockchain.request.CsvRequest;

public interface TransactionService {

	Transaction createTransaction(CsvRequest request, User user) throws Exception;

	String createNewTransaction(CsvRequest request) throws Exception;

	Transaction getTransactionByHash(String hash);

	List<Transaction> getTransactionHistory(String client);

}
