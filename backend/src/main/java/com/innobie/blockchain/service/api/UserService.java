package com.innobie.blockchain.service.api;

import com.innobie.blockchain.entity.User;
import com.innobie.blockchain.request.CsvRequest;

public interface UserService {

	User createUser(CsvRequest request);

	User getClientById(Integer client);

}
