package com.innobie.blockchain.service.impl;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.innobie.blockchain.entity.Block;
import com.innobie.blockchain.entity.Transaction;
import com.innobie.blockchain.repository.BlockRepository;
import com.innobie.blockchain.request.CsvRequest;
import com.innobie.blockchain.service.api.BlockService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BlockServiceImpl implements BlockService {

	@Value("${app.home}")
	private String path;

	@Autowired
	private BlockRepository blockRepository;

	@Override
	public Block initBlock(CsvRequest request, Transaction transaction) throws Exception {
		Block block = new Block();
		block.setTransaction(transaction);
		if (!blockRepository.findById(1).isPresent()) {
			block.setPreviousHash("0");
			block.setHash(calculateHash(block));
			blockRepository.save(block);
			return block;
		} else {
			Block prevBlock = blockRepository.findFirstByOrderByBlockIdDesc();
			block.setPreviousHash(prevBlock.getHash());
			block.setHash(calculateHash(block));
			// shouldn't need validation here, because the algorithm won't fail and it slows down the generation a lot,
			// if someone changes the database during
			// the csv import, it'll be validated at the next block creation
			// if (validateBlock()) {
			blockRepository.save(block);
			return block;
			// }
		}
		// throw new RuntimeException("Blockchain is invalid!");
	}

	@Override
	public Block createBlock(CsvRequest request, Transaction transaction) throws Exception {
		Block block = new Block();
		block.setTransaction(transaction);
		if (!blockRepository.findById(1).isPresent()) {
			block.setPreviousHash("0");
			block.setHash(calculateHash(block));
			blockRepository.save(block);
			log(block);
			return block;
		} else {
			Block prevBlock = blockRepository.findFirstByOrderByBlockIdDesc();
			block.setPreviousHash(prevBlock.getHash());
			block.setHash(calculateHash(block));
			if (validateBlock()) {
				log.info("Blockchain is validated!");
				blockRepository.save(block);
				log(block);
				return block;
			}
		}
		throw new RuntimeException("Blockchain is invalid!");
	}

	private String calculateHash(Block block) {
		String hash = DigestUtils.sha256Hex(
				block.getPreviousHash() + Long.toString(block.getTransaction().getTransactionTime().getTime())
						+ Integer.toString(block.getTransaction().getPoints()));
		return hash;
	}

	private boolean validateBlock() {
		List<Block> blockchain = blockRepository.findAll();
		for (int i = 1; i < blockchain.size(); i++) {
			if (!blockchain.get(i).getHash().equals(calculateHash(blockchain.get(i)))) {
				return false;
			}
			if (!blockchain.get(i - 1).getHash().equals(blockchain.get(i).getPreviousHash())) {
				return false;
			}
		}
		return true;
	}

	private void log(Block block) {
		try {
			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(path + "/" + block.getHash() + ".csv"), "UTF-8"));
			StringBuffer oneLine = new StringBuffer();
			oneLine.append(block.getBlockId());
			oneLine.append(";");
			oneLine.append(block.getHash());
			oneLine.append(";");
			oneLine.append(block.getPreviousHash());
			bw.write(oneLine.toString());
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
