package com.innobie.blockchain.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.innobie.blockchain.entity.Transaction;
import com.innobie.blockchain.entity.User;
import com.innobie.blockchain.request.CsvRequest;
import com.innobie.blockchain.service.api.BlockService;
import com.innobie.blockchain.service.api.CsvHandlerService;
import com.innobie.blockchain.service.api.TransactionService;
import com.innobie.blockchain.service.api.UserService;

@Service
public class CsvHandlerServiceImpl implements CsvHandlerService {

	@Autowired
	private UserService userService;

	@Autowired
	private TransactionService transactionService;

	@Autowired
	private BlockService blockService;

	@Override
	public List<CsvRequest> readFile(MultipartFile file) throws Exception {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		ArrayList<String> arr = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(convFile))) {
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				arr.add(sCurrentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		List<CsvRequest> list = new ArrayList<>();
		for (String line : arr) {
			list.add(new CsvRequest(line));
		}
		for (CsvRequest csvRequest : list) {
			User user = userService.createUser(csvRequest);
			if (user != null) {
				Transaction transaction = transactionService.createTransaction(csvRequest, user);
				if (transaction != null) {
					blockService.initBlock(csvRequest, transaction);
				}
			}
		}
		convFile.delete();

		return null;
	}

}
