package com.innobie.blockchain.service.impl;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innobie.blockchain.entity.Block;
import com.innobie.blockchain.entity.Transaction;
import com.innobie.blockchain.entity.User;
import com.innobie.blockchain.repository.BlockRepository;
import com.innobie.blockchain.repository.TransactionRepository;
import com.innobie.blockchain.repository.UserRepository;
import com.innobie.blockchain.request.CsvRequest;
import com.innobie.blockchain.service.api.BlockService;
import com.innobie.blockchain.service.api.TransactionService;
import com.innobie.blockchain.service.api.UserService;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private BlockRepository blockRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private BlockService blockService;

	@Override
	public Transaction createTransaction(CsvRequest request, User user) throws Exception {
		Transaction transaction = new Transaction();
		transaction.setUser(user);
		transaction.setPoints(Integer.parseInt(request.getPoints()));
		transaction.setTransactionTime(new SimpleDateFormat("yyyy.MM.dd hh:mm").parse(request.getTransactionTime()));
		transactionRepository.save(transaction);
		return transaction;
	}

	@Override
	public String createNewTransaction(CsvRequest request) throws Exception {
		User user = userService.createUser(request);
		if (user != null) {
			Transaction transaction = createTransaction(request, user);
			if (transaction != null) {
				Block block = blockService.createBlock(request, transaction);
				return block.getHash();
			}
		}
		return null;
	}

	@Override
	public Transaction getTransactionByHash(String hash) {
		Optional<Block> block = blockRepository.findByHash(hash);
		if (block.isPresent()) {
			return block.get().getTransaction();
		}
		return null;
	}

	@Override
	public List<Transaction> getTransactionHistory(String client) {
		List<Transaction> list = transactionRepository
				.findAllByUser(userRepository.findByClientId(Integer.parseInt(client)).get());
		return list;
	}

}
