package com.innobie.blockchain.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innobie.blockchain.entity.User;
import com.innobie.blockchain.repository.UserRepository;
import com.innobie.blockchain.request.CsvRequest;
import com.innobie.blockchain.service.api.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User createUser(CsvRequest request) {
		Optional<User> optional = userRepository.findById(Integer.parseInt(request.getClientId()));
		if (!optional.isPresent()) {
			if (Integer.parseInt(request.getPoints()) >= 0) {
				User user = new User();
				user.setClientId(Integer.parseInt(request.getClientId()));
				user.setBalance(Long.parseLong(request.getPoints()));
				user.setData(null);
				userRepository.save(user);
				return user;
			} else {
				return null;
			}
		}
		if (optional.get().getBalance() + Integer.parseInt(request.getPoints()) >= 0) {
			optional.get().setBalance(optional.get().getBalance() + Long.parseLong(request.getPoints()));
			userRepository.save(optional.get());
		} else {
			return null;
		}
		return optional.get();
	}

	@Override
	public User getClientById(Integer client) {
		Optional<User> user = userRepository.findByClientId(client);
		if (user.isPresent()) {
			return user.get();
		}
		return null;
	}

}
