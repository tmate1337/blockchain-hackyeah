import React, { Component } from 'react';
import { Col, Row, Input, Form, Button, DatePicker  } from 'antd';
import LoadingIndicator  from '../common/LoadingIndicator';
import './Add.css';
import NotFound from '../common/NotFound';
import AuthRequired from '../common/AuthRequired';
import ServerError from '../common/ServerError';
import { API_BASE_URL } from '../constant';

const FormItem = Form.Item;

class Add extends Component {
    constructor(props) {
      super(props);
        this.state = {
            hash: '',
            isLoading: false
        }
    }

    generateTransaction = (e) => {
        e.preventDefault();
        fetch(API_BASE_URL + "/transaction/", {
         method: 'post',
         headers: {'Content-Type':'application/json'},
         body: JSON.stringify({transactionTime: e.target[0].value, clientId:e.target[1].value, points: e.target[2].value})})
         .then(res => res.text())
         .then(
           (result) => {
             this.setState({
               isLoading: false,
               hash: result
             });
           },
           (error) => {
             this.setState({
               isLoading: false
             });
           }
         )
    }

    render() {

        if(this.state.isLoading) {
            return <LoadingIndicator />;
        }

        if(this.state.notFound) {
            return <NotFound />;
        }

        if(this.state.authRequired) {
            return <AuthRequired />;
        }

        if(this.state.serverError) {
            return <ServerError />;
        }

        return (
          <div className="add">
            <h1>Create a transaction</h1>
                <Form layout="inline" onSubmit={this.generateTransaction}>
                  <FormItem>
                    <DatePicker
                    name="date"
                    showTime
                    format="YYYY.MM.DD HH:mm"
                    placeholder="Select Date"
                  />
                  </FormItem>
                  <FormItem>
                    <Input type="text" placeholder="Client ID" name="client"/>
                  </FormItem>
                  <FormItem>
                    <Input type="text" placeholder="Points" name="points"/>
                  </FormItem>
                  <FormItem>
                    <Button
                      type="primary"
                      htmlType="submit">
                      Create
                    </Button>
                  </FormItem>
                </Form>
                {
                  this.state.hash ? (
                    <div>
                    <Col>
                      <Row><h3>Generated hash</h3></Row>
                      <Row>{this.state.hash}</Row>
                    </Col>
                    </div>
                  ): null
              }
          </div>
        );
    }
}

export default Add;
