import React, { Component } from 'react';
import './App.css';
import {
  Route,
  withRouter,
  Switch
} from 'react-router-dom';


import AppHeader from '../common/AppHeader';
import NotFound from '../common/NotFound';
import LoadingIndicator from '../common/LoadingIndicator';
import Home from '../common/Home';
import Client from '../client/Client';
import Add from '../add/Add';
import Hash from '../hash/Hash';
import Csv from '../csv/Csv';

import { Layout, notification } from 'antd';
const { Content } = Layout;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    }

    notification.config({
      placement: 'topRight',
      top: 70,
      duration: 3,
    });
  }

  render() {
    if(this.state.isLoading) {
      return <LoadingIndicator />
    }
    return (
        <Layout className="app-container">
          <AppHeader/>
          <Content className="app-content">
            <div className="container">
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/client" component={Client} />
                <Route exact path="/add" component={Add} />
                <Route exact path="/hash" component={Hash} />
                <Route exact path="/csv" component={Csv} />
                <Route component={NotFound}></Route>
              </Switch>
            </div>
          </Content>
        </Layout>
    );
  }
}

export default withRouter(App);
