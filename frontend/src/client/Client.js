import React, { Component } from 'react';
import { Col, Row, BackTop, Input, Table } from 'antd';
import LoadingIndicator  from '../common/LoadingIndicator';
import './Client.css';
import NotFound from '../common/NotFound';
import AuthRequired from '../common/AuthRequired';
import ServerError from '../common/ServerError';
import { API_BASE_URL } from '../constant';

const Search = Input.Search;

const columns = [{
  title: 'ID',
  dataIndex: 'transactionId',
  key: 'transactionId',
}, {
  title: 'Date',
  dataIndex: 'transactionTime',
  key: 'transactionTime',
}, {
  title: 'Points',
  dataIndex: 'points',
  key: 'points',
}];

class Client extends Component {
    constructor(props) {
      super(props);
        this.state = {
            search: [],
            history: [],
            isLoading: false
        }
    }

    search(id){
      fetch(API_BASE_URL + "/user/" + id)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoading: false,
            search: result
          });
        },
        (error) => {
          this.setState({
            isLoading: false
          });
        }
      )
      }

      searchHistory(id){
        fetch(API_BASE_URL + "/transaction/history/" + id)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              isLoading: false,
              history: result
            });
          },
          (error) => {
            this.setState({
              isLoading: false
            });
          }
        )
        }

    render() {

        if(this.state.isLoading) {
            return <LoadingIndicator />;
        }

        if(this.state.notFound) {
            return <NotFound />;
        }

        if(this.state.authRequired) {
            return <AuthRequired />;
        }

        if(this.state.serverError) {
            return <ServerError />;
        }

        return (
          <div className="client">
            <BackTop/>
            <Row>
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
              <h1>Balance check</h1>
                  <Search
                    className="search-box"
                    placeholder="Client id"
                    onSearch={value => this.search(value)}
                    style={{ width: 200 }}
                  />
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
              <h1>History check</h1>
                  <Search
                    className="search-box"
                    placeholder="Client id"
                    onSearch={value => this.searchHistory(value)}
                    style={{ width: 200 }}
                  />
              </Col>
            </Row>
            <Row>
            <Col sxs={24} sm={24} md={12} lg={12} xl={12}>
              {
                this.state.search.clientId ? (
                  <div className="search-data">
                    <h3>ID</h3>
                    <p>{this.state.search.clientId}</p>
                    <h3>Balance</h3>
                    <p>{this.state.search.balance} points</p>
                  </div>
                ): null
              }
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
              {
                this.state.history ? (
                  <div>
                  <Table className="table" columns={columns} dataSource={this.state.history}/>
                  </div>
                ): null
              }
              </Col>
            </Row>
          </div>
        );
    }
}

export default Client;
