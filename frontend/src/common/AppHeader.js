import React, { Component } from 'react';
import {
    Link,
    withRouter
} from 'react-router-dom';
import './AppHeader.css';
import { Layout, Menu, Icon } from 'antd';
const Header = Layout.Header;

class AppHeader extends Component {

    render() {
        let menuItems;
          menuItems = [
            <Menu.Item key="/">
              <Link to="/">
                <Icon type="home" className="nav-icon" />
              </Link>
            </Menu.Item>,
            <Menu.Item key="/client">
              <Link to="/client">
                <Icon type="file-search" className="nav-icon" />
              </Link>
            </Menu.Item>,
            <Menu.Item key="/add">
              <Link to="/add">
                <Icon type="plus" className="nav-icon" />
              </Link>
            </Menu.Item>,
            <Menu.Item key="/hash">
              <Link to="/hash">
                <Icon type="fund" className="nav-icon" />
              </Link>
            </Menu.Item>,
            <Menu.Item key="/csv">
              <Link to="/csv">
                <Icon type="cloud-upload" className="nav-icon" />
              </Link>
            </Menu.Item>
          ];

        return (
            <Header className="app-header">
            <div className="container">
              <div className="app-title" >
                <Link to="/">Blockchain</Link>
              </div>
              <Menu
                className="app-menu"
                mode="horizontal"
                selectedKeys={[this.props.location.pathname]}
                style={{ lineHeight: '64px' }} >
                  {menuItems}
              </Menu>
            </div>
          </Header>
        );
    }
}

export default withRouter(AppHeader);
