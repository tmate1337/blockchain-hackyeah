import React, { Component } from 'react';
import { Icon } from 'antd';
import { Link } from 'react-router-dom';
import './Home.css';

class Home extends Component {

    render() {
        return (
            <div className="home-page">
              <h1 className="home-header">Home page</h1>
              <h2>
              <Link to="/client">
                <Icon type="file-search" className="nav-icon" style={{ fontSize: '32px', marginRight: '10px'}} />
              </Link>
              Check your balance, or your transaction history!
              </h2>
              <h2>
              <Link to="/add">
                <Icon type="plus" className="nav-icon" style={{ fontSize: '32px', marginRight: '10px'}} />
              </Link>
              Create a new transaction!
              </h2>
              <h2>
              <Link to="/hash">
                <Icon type="fund" className="nav-icon" style={{ fontSize: '32px', marginRight: '10px'}}/>
              </Link>
              Get a transaction by hash!
              </h2>
              <h2>
              <Link to="/csv">
                <Icon type="cloud-upload" className="nav-icon" style={{ fontSize: '32px', marginRight: '10px'}}/>
              </Link>
              Upload a csv file!
              </h2>
            </div>
        );
    }
}


export default Home;
