import React, { Component } from 'react';
import { Form, Upload, Icon } from 'antd';
import LoadingIndicator  from '../common/LoadingIndicator';
import './Csv.css';

import NotFound from '../common/NotFound';
import AuthRequired from '../common/AuthRequired';
import ServerError from '../common/ServerError';
import { API_BASE_URL } from '../constant';

const FormItem = Form.Item;

class Csv extends Component {
    constructor(props) {
      super(props);
        this.state = {
            isLoading: false
        }
    }

    render() {

        if(this.state.isLoading) {
            return <LoadingIndicator />;
        }

        if(this.state.notFound) {
            return <NotFound />;
        }

        if(this.state.authRequired) {
            return <AuthRequired />;
        }

        if(this.state.serverError) {
            return <ServerError />;
        }

        return (
          <div className="csv">
            <h1>Upload a csv</h1>
            <Form layout="inline">
              <FormItem>
              <Upload.Dragger name="file" action={API_BASE_URL + "/csv/upload"}>
                <p className="ant-upload-drag-icon">
                  <Icon type="inbox" />
                </p>
                <p className="ant-upload-text">Click or drag file to this area to upload</p>
              </Upload.Dragger>
              </FormItem>
            </Form>
          </div>
        );
    }
}

export default Csv;
