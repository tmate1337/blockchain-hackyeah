import React, { Component } from 'react';
import { Col, Row, Input } from 'antd';
import LoadingIndicator  from '../common/LoadingIndicator';
import './Hash.css';
import NotFound from '../common/NotFound';
import AuthRequired from '../common/AuthRequired';
import ServerError from '../common/ServerError';
import { API_BASE_URL } from '../constant';
import Moment from 'react-moment';

const Search = Input.Search;

class Hash extends Component {
    constructor(props) {
      super(props);
        this.state = {
            search: [],
            isLoading: false
        }
    }

    search(hash){
      fetch(API_BASE_URL + "/transaction/" + hash)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoading: false,
            search: result
          });
        },
        (error) => {
          this.setState({
            isLoading: false
          });
        }
      )
      }

    render() {

        if(this.state.isLoading) {
            return <LoadingIndicator />;
        }

        if(this.state.notFound) {
            return <NotFound />;
        }

        if(this.state.authRequired) {
            return <AuthRequired />;
        }

        if(this.state.serverError) {
            return <ServerError />;
        }

        return (
          <div className="hash">
            <h1>Get a transaction by hash</h1>
                <Search
                  className="search-box"
                  placeholder="Hash"
                  onSearch={value => this.search(value)}
                  style={{ width: 200 }}
                />
                {
                  this.state.search.transactionId ? (
                    <div>
                    <Col>
                      <Row><h3>Transaction ID</h3></Row>
                      <Row>{this.state.search.transactionId}</Row>
                    </Col>
                    <Col>
                      <Row><h3>Transaction time</h3></Row>
                      <Row><Moment format="YYYY.MM.DD HH:mm">{this.state.search.transactionTime}</Moment></Row>
                    </Col>
                    <Col>
                      <Row><h3>Points</h3></Row>
                      <Row>{this.state.search.points}</Row>
                    </Col>
                    </div>
                  ): null
              }
          </div>
        );
    }
}

export default Hash;
